# CHANGELOG

## Unreleased

## 4.3.0 - 2023-03-09

- Update button focus style

## 4.2.2 - 2022-07-27

- Ensure ratio are block element

## 4.2.1 - 2022-04-20

- Restore content alignement in main axis for button

## 4.2.0 - 2022-03-24

- Use `@layer` everywhere

## 4.1.0 - 2022-02-22

- Add default square ratio

## 4.0.0 - 2021-01-24

- Refactor to propose a CSS ans Scss version
- Use CSS Custom Property everywhere

## 3.4.1 - 2021-12-21

- Restore content alignement in main axis for button

## 3.4.0 - 2021-12-21

- Add Y gutter to grid
- Use Custom Property for container
- Remove screens variants from container
- Remove page and headings from base
- Remove icon and gutter from components
- Support tailwindcss 3.x

## 3.3.0 - 2021-09-07

- Fix an issue with `c-ratio c-ratio----none` that can't be reverse

## 3.2.0 - 2021-06-21

- Use smooth scroll by default
- Update to tailwind 2.2.x

## 3.1.0 - 2021-04-14

- Move switch to it's own component
- Add reset with global prefers-reduced-motion
- Add `form-theme` setting
- Fix tailwind ring compatibility for `button`, `input`, `toggle` and `switch`
- Update to tailwindcss 2.1.x 

## 3.0.0 - 2021-03-08

- Use blue instead of indigo for ring button color

## 3.0.0-alpha.9 - 2021-02-09

- Add scroll-behavior
- Deprecated `alert`

## 3.0.0-alpha.8 - 2020-11-19

- Fix `button`, `toggle` ring 
- Update tailwindcss to 2.0.1 

## 3.0.0-alpha.7 - 2020-11-18

- Fix `button`, `input` and `toggle` ring 
- Update tailwindcss to 2.0.0-alpha.25

## 3.0.0-alpha.6 - 2020-11-12

- Fix `button`, `input` and `toggle` ring 
- Update tailwindcss to 2.0.0-alpha.23 

## 3.0.0-alpha.5 - 2020-11-12

- Update `button`, `input` and `toggle` style
- Update tailwindcss to 2.0.0-alpha.15

## 3.0.0-alpha.3 - 2020-11-11

- Update `button`, `input` and `toggle` style
- Update tailwindcss to 2.0.0-alpha.13

## 3.0.0-alpha.2 - 2020-11-09

- Add `ratio--none` to remove any aspect ratio behavior 
- Update tailwindcss to 2.0.0-alpha.9 

## 3.0.0-alpha.1 - 2020-10-22

- Update tailwindcss to 2.0.0-alpha.1 

## 2.4.0 - 2020-10-20

- Add `width: 100%` to container to fix some issue with flexbox on parrent element

## 2.3.0 - 2020-10-04

- Remove `fill: currentColor` from `icon`, use `fill-current` and `stroke-current` utilities instead

## 2.2.0 - 2020-10-02

- Add `3:4` ratio

## 2.1.0 - 2020-08-26

- Move framework to `css` folder

## 2.0.0 - 2020-08-12

- Remove `separator`
- Move `gutter` to its own component (and rename classes)
- Refactor truncate component
- Use default separator for `list-inline-sep`
- Use CSS Custom Properties for `button`, `grid`, `gutter`, `ratio` and `truncate` to allow customization via style
- Use info for default alert theme and add warning theme
