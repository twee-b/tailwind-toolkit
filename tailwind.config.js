const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    'src/templates/**/*.html',
  ],
  theme: {
    extend: {
      fontSize: {
        xs2: ['0.75rem', '1.33333'],
        sm2: ['0.875rem', '1.43'],
        base2: ['1rem', '1.5'],
        lg2: ['1.125rem', '1.55555'],
        xl2: ['1.25rem', '1.4'],
        '2xl2': ['1.5rem', '1.33333'],
        '3xl2': ['1.875rem', '1.2'],
        '4xl2': ['2.25rem', '1.11111'],
      }
    },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/typography'),
  ],
}
